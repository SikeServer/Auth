package com.sikeserver.auth.command;

import java.sql.SQLException;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.auth.AuthManager;
import com.sikeserver.auth.util.AuthCode;
import com.sikeserver.core.CoreManager;

public class MyAuthCode implements CommandExecutor {
	private AuthManager plugin;
	private CoreManager core;
	
	public MyAuthCode() {
		plugin = AuthManager.getPlugin();
		core = plugin.core;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// コンソールからの実行でないかチェック
		if (!(sender instanceof Player)) {
			sender.sendMessage(core.getMessage("Error.Common.Console"));
			return true;
		}
		
		Player p = (Player)sender;
		
		String authCode = null;
		try {
			authCode = AuthCode.getAuthCode(p.getUniqueId());
		} catch (SQLException e) {
			p.sendMessage(core.getMessage("Error.Common.SQL"));
			e.printStackTrace();
			return true;
		}
		
		if (authCode == null) {
			p.sendMessage(core.getMessage("Message.Auth.Authed"));
			return true;
		} else {
			List<String> messages = core.getMessageList("Message.Auth.MyCode");
			for (String msg : messages) {
				p.sendMessage(
					ChatColor.translateAlternateColorCodes('&', msg)
						.replaceAll("%authcode%", authCode)
				);
			}
		}
		
		return true;
	}
}
