package com.sikeserver.auth;

import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.auth.command.MyAuthCode;
import com.sikeserver.core.CoreManager;

public class AuthManager extends JavaPlugin {
	private static AuthManager plugin;
	public CoreManager core;
	
	@Override
	public void onEnable() {
		plugin = this;
		core = CoreManager.getPlugin();
		
		this.getCommand("auth").setExecutor(new MyAuthCode());
	}
	
	public static AuthManager getPlugin() {
		return plugin;
	}
}
