package com.sikeserver.auth.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;

import javax.xml.bind.DatatypeConverter;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.sikeserver.auth.AuthManager;
import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.SQLManager;

public class AuthCode {
	/**
	 * 新しい認証コードを生成します。
	 * 
	 * @author Siketyan
	 * 
	 * @return 新しい認証コード
	 */
	public static String generateAuthCode() {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			AuthManager.getPlugin().getLogger().warning("Failed generate authenification code.");
			e.printStackTrace();
		}
		byte[] result = md5.digest(String.valueOf(System.nanoTime()).getBytes());
		return DatatypeConverter.printHexBinary(result).substring(0, 16);
	}
	
	/**
	 * 指定したプレイヤーに認証コードを通知します。
	 * すでに認証済みであれば通知しません。
	 * 
	 * @author Siketyan
	 * 
	 * @param p 通知するプレイヤー
	 */
	public static void noticeAuth(Player p) throws SQLException {
		CoreManager core = AuthManager.getPlugin().core;
		
		String authCode = getAuthCode(p.getUniqueId());
		if (authCode == null) return; // 認証済みならreturn
		
		List<String> messages = core.getMessageList("Message.Auth.Notice");
		for (String msg : messages) {
			p.sendMessage(
				ChatColor.translateAlternateColorCodes('&', msg)
					.replaceAll("%authcode%", authCode)
			);
		}
	}
	
	/**
	 * 指定したプレイヤーの認証コードを取得します。
	 * 
	 * @author Siketyan
	 * 
	 * @param u 対象プレイヤー
	 * 
	 * @return 認証コード。認証済みであればnull
	 */
	public static String getAuthCode(UUID u) throws SQLException{
		SQLManager sql = AuthManager.getPlugin().core.sql;
		
		try (Statement stmt = sql.getStatement();
			 ResultSet result = stmt.executeQuery(
				"SELECT * FROM " + CoreManager.playerTable
					+ " WHERE uuid = \"" + u.toString() + "\""
			 ))
		{
			// 最初の行にセット
			result.next();
			if (result.getInt("authed") == 1) { // 認証済みなら
				return null; // returnで抜ける
			} else { // そうでなければ（未認証なら）
				return result.getString("authcode"); // 認証コードを返す
			}
		}
	}
}
